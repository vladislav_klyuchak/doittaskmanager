package com.vkluchak.doittaskmanager.vo

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*


/**
 * Return date in specified format.
 * @param seconds Date in seconds
 * @param dateFormat Date format
 * @return String representing date in specified format
 */
@SuppressLint("SimpleDateFormat")
fun getDate(seconds : Long, dateFormat: String = "dd/MM/yyyy kk:mm") : String
{
    // Create a DateFormatter object for displaying date in specified format.
    val formatter = SimpleDateFormat(dateFormat)

    // Create a calendar object that will convert the date and time value in milliseconds to date.
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = (seconds * 1000L)
    return formatter.format(calendar.time)
}