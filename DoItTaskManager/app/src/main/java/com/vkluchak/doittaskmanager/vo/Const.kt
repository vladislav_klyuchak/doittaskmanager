package com.vkluchak.doittaskmanager.vo

const val SORT_ORDER_ASC = "ASC"
const val SORT_ORDER_DESC = "DESC"
const val SORT_TYPE_DATE = "dueBy"
const val SORT_TYPE_PRIORITY = "priority"
const val SORT_TYPE_NAME = "title"