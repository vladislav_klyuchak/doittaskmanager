package com.vkluchak.doittaskmanager.repository.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.vkluchak.doittaskmanager.api.Task
import com.vkluchak.doittaskmanager.repository.TaskRepository
import io.reactivex.disposables.CompositeDisposable

/**
 *
 * A simple data source factory which also provides a way to observe the last created data source.
 * This allows us to channel its network request status etc back to the UI. See the Listing creation
 * in the Repository class.
 */
class TasksDataSourceFactory(
    private val compositeDisposable: CompositeDisposable,
    private val taskRepository: TaskRepository
) : DataSource.Factory<Int, Task>() {

    val tasksDataSourceLiveData = MutableLiveData<PageKeyedTaskDataSource>()

    override fun create(): DataSource<Int, Task> {
        val usersDataSource = PageKeyedTaskDataSource(taskRepository, compositeDisposable)
        tasksDataSourceLiveData.postValue(usersDataSource)
        return usersDataSource
    }

}
