package com.vkluchak.doittaskmanager.api.network.interceptor

import okhttp3.Response

interface ResponseChainHandler {
    fun canHandle(response: Response): Boolean

    fun chain(builder: Response.Builder) : Response.Builder

}