package com.vkluchak.doittaskmanager.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.vkluchak.doittaskmanager.api.Task
import com.vkluchak.doittaskmanager.repository.TaskRepository
import com.vkluchak.doittaskmanager.repository.datasource.TasksDataSourceFactory
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class TaskListViewModel @Inject constructor(private val taskRepository: TaskRepository) : ViewModel() {

    private val pageSize = 15
    private val compositeDisposable = CompositeDisposable()
    private val sourceFactory: TasksDataSourceFactory
    var tasksList: LiveData<PagedList<Task>>

    private var sortValue: Pair<String, String> = Pair("", "")

    fun setListSort(first: String = sortValue.first, second: String = sortValue.second) {
        sortValue =  Pair(first, second)
        taskRepository.setSortTasksParam(sortValue)
        refresh()
    }

    init {
        sourceFactory = TasksDataSourceFactory(compositeDisposable, taskRepository)
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 2)
            .setEnablePlaceholders(false)
            .build()
        tasksList = sourceFactory.toLiveData(config = config)

    }

    fun retry() {
        sourceFactory.tasksDataSourceLiveData.value?.retry()
    }

    fun refresh() {
        sourceFactory.tasksDataSourceLiveData.value?.invalidate()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}