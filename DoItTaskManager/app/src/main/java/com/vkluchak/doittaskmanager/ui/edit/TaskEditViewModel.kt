package com.vkluchak.doittaskmanager.ui.edit

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.ReplayRelay
import com.vkluchak.doittaskmanager.api.Task
import com.vkluchak.doittaskmanager.repository.TaskRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TaskEditViewModel @Inject constructor(private val taskRepository: TaskRepository) : ViewModel() {

    val errorReplayRelay: ReplayRelay<String> = ReplayRelay.create()
    val taskProfileRelay: PublishRelay<Task> = PublishRelay.create()
    val updatedStatusRelay: PublishRelay<Boolean> = PublishRelay.create()

    fun getTask(taskId : Int){
        taskRepository.getTask(taskId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                errorReplayRelay.accept(it.message)
            }
            .doOnSuccess { taskProfileRelay.accept(it) }
            .subscribe()
    }

    fun updateTask(taskId: Int, pikedTime: Long, priority: String, taskTitle: String) {
        taskRepository.updateTask(taskId, pikedTime,priority, taskTitle )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                errorReplayRelay.accept(it.message)
            }
            .doOnComplete { updatedStatusRelay.accept(true) }
            .subscribe()
    }
}