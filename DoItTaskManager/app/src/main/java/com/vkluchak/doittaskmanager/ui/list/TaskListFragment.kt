package com.vkluchak.doittaskmanager.ui.list

import android.os.Bundle
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vkluchak.doittaskmanager.di.Injectable
import com.vkluchak.doittaskmanager.R
import com.vkluchak.doittaskmanager.api.Task
import com.vkluchak.doittaskmanager.databinding.FragmentTaskListBinding
import com.vkluchak.doittaskmanager.ui.info.ARG_TASK_ID
import com.vkluchak.doittaskmanager.vo.*
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class TaskListFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var taskAdapter: TaskAdapter
    private lateinit var binding: FragmentTaskListBinding
    private lateinit var navController: NavController
    private lateinit var model: TaskListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTaskListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_list_filter, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        item.isChecked = true

        when (item.itemId) {
            R.id.miName -> {
                model.setListSort(first = SORT_TYPE_NAME)
            }
            R.id.miPriority -> {
                model.setListSort(first = SORT_TYPE_PRIORITY)
            }
            R.id.miDate -> {
                model.setListSort(first = SORT_TYPE_DATE)
            }
            R.id.miASC ->{
                model.setListSort(second =  SORT_ORDER_ASC)
            }
            R.id.miDESC -> {
                model.setListSort(second =  SORT_ORDER_DESC)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        model = ViewModelProviders.of(this, viewModelFactory)
            .get(TaskListViewModel::class.java)

        model.tasksList.observe(this, Observer<PagedList<Task>> {
            taskAdapter.submitList(it)
            binding.sRefreshLayout.isRefreshing = false
        })

        initAdapter()

        binding.fabCreateTask.setOnClickListener {
            navController.navigate(R.id.action_list_screen_to_create)
        }

        binding.sRefreshLayout.setOnRefreshListener {
            requestData()
        }

        taskAdapter.onClickSubjectRelay()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                val bundle = bundleOf(ARG_TASK_ID to it.id)
                this.findNavController().navigate(R.id.action_list_to_info, bundle)
            }.subscribe()

    }

    override fun onResume() {
        super.onResume()
        requestData()
    }

    private fun requestData() {
        model.refresh()
    }

    private fun initAdapter() {
        val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        taskAdapter = TaskAdapter {
            model.retry()
        }
        binding.rvList.layoutManager = linearLayoutManager
        binding.rvList.adapter = taskAdapter
        model.tasksList.observe(this, Observer<PagedList<Task>> { taskAdapter.submitList(it) })
    }

}