package com.vkluchak.doittaskmanager.ui.create

import android.content.res.Resources
import android.icu.util.Calendar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.vkluchak.doittaskmanager.R
import com.vkluchak.doittaskmanager.databinding.FragmentTaskCreateBinding
import com.vkluchak.doittaskmanager.di.Injectable
import com.vkluchak.doittaskmanager.vo.getDueByTimeSeconds
import com.vkluchak.doittaskmanager.vo.initDateTimeViewsValues
import com.vkluchak.doittaskmanager.vo.showToast
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject


class CreateTaskFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentTaskCreateBinding

    private lateinit var model: CreateTaskViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTaskCreateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model = ViewModelProviders.of(this, viewModelFactory)
            .get(CreateTaskViewModel::class.java)
        
        subscribeToModelData(Navigation.findNavController(view))
        
        setUpViews()
    }

    private fun setUpViews() {
        binding.dpTaskDate.findViewById<View>(
            Resources.getSystem()
                .getIdentifier("year", "id", "android")
        ).visibility = View.GONE

        binding.tpTaskTime.setIs24HourView(true)
        
        binding.btnCreateEvent.setOnClickListener {
            val priority = when (binding.rgTaskPriority.checkedRadioButtonId) {
                R.id.rbHigh -> "High"
                R.id.rbMedium -> "Normal"
                R.id.rbLow -> "Low"
                else -> "Low"
            }
            val pikedTime = getDueByTimeSeconds(binding.dpTaskDate, binding.tpTaskTime)
            if(pikedTime > System.currentTimeMillis())
                model.createTask(
                    pikedTime,
                    priority,
                    binding.etTaskTitle.text.toString()
                )else {
                context?.showToast("Task time should be greater than the current ")
            }
        }

        initDateTimeViewsValues(binding.dpTaskDate, binding.tpTaskTime)
    }

    private fun subscribeToModelData(navController : NavController) {
        model.errorReplayRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                context?.showToast("Error \n $it")
            }.subscribe()

        model.successProfileRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                navController.popBackStack()
            }.subscribe()


    }



}