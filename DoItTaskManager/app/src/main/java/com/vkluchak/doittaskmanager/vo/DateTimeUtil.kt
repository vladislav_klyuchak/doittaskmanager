package com.vkluchak.doittaskmanager.vo

import android.icu.util.Calendar
import android.widget.DatePicker
import android.widget.TimePicker

fun getDueByTimeSeconds(datePicker: DatePicker, timePicker: TimePicker): Long {

    val calendar = Calendar.getInstance()
    calendar.set(
        datePicker.year, datePicker.month, datePicker.dayOfMonth,
        0, timePicker.minute, 0
    )
    // set hour_of_Day 24h instead hour 12h am/pm
    calendar.set(Calendar.HOUR_OF_DAY, timePicker.hour)

    return calendar.timeInMillis
}

fun initDateTimeViewsValues(datePicker: DatePicker, timePicker: TimePicker, dueBySeconds: Long? = null){

    val c = Calendar.getInstance()
    dueBySeconds?.let {
        //convert seconds to milliseconds
        c.timeInMillis = it * 1000
    }
    val year = c.get(Calendar.YEAR)
    val month = c.get(Calendar.MONTH)
    val day = c.get(Calendar.DAY_OF_MONTH)
    val hour = c.get(Calendar.HOUR_OF_DAY)
    val minute = c.get(Calendar.MINUTE)

    // set current date into datePicker
    datePicker.init(year, month, day, null)
    datePicker.minDate = System.currentTimeMillis()

    timePicker.hour = hour
    timePicker.minute = minute
}