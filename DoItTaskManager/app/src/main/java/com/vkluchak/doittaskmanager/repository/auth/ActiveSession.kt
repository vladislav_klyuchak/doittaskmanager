package com.vkluchak.doittaskmanager.repository.auth


/**
 * <p>
 *     This class represents user session
 * </p>
 */

sealed class Session {
    /**
     * Returns true if current session is active and user is authorized
     */
    abstract val isAuthorized: Boolean

    val isNotAuthorized: Boolean
        get() = !isAuthorized
}

class InactiveSession private constructor() : Session() {
    override val isAuthorized = false

    companion object {
        val INSTANCE = InactiveSession()
    }
}


data class ActiveSession(val accessToken: Token) : Session() {

    companion object {
        fun newSession(accessToken: Token) =
            ActiveSession(accessToken)
    }

    override val isAuthorized: Boolean
        get() = accessToken != null

}

data class Token(val value: String) {
    init {
        require(value.isNotBlank())
    }
}
