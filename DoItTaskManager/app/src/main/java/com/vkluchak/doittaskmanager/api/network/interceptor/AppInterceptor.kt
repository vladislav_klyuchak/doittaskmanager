package com.vkluchak.doittaskmanager.api.network.interceptor

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AppInterceptor(collection: Collection<RequestChainHandler>) : Interceptor {

    private val interceptors = ArrayList<RequestChainHandler>(collection)

    fun addInterceptor(handler: RequestChainHandler) {
        synchronized(interceptors) {
            interceptors += handler
        }
    }

    fun removeInterceptor(handler: RequestChainHandler) {
        synchronized(interceptors) {
            interceptors.remove(handler)
        }
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        if (canGetFromCache(chain.request())) {
            modifyDefault(requestBuilder)
        }

        val updated = synchronized(interceptors) {
            val builder = interceptors.filter { it.canHandle(chain.request()) }
                    .fold(requestBuilder) { b, handler -> handler.chain(b) }

            return@synchronized builder?.build() ?: requestBuilder.build()
        }

        return chain.proceed(updated)
    }

    private fun modifyDefault(builder: Request.Builder) {
        builder
                .removeHeader("Pragma")
                .removeHeader("Access-Control-Allow-Origin")
                .removeHeader("Vary")
                .removeHeader("Cache-Control")
    }

    private fun canGetFromCache(request: Request) = "GET".equals(request.method(), ignoreCase = true)

}