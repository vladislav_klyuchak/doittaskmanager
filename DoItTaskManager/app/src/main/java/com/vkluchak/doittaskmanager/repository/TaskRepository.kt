package com.vkluchak.doittaskmanager.repository

import com.jakewharton.rxrelay2.BehaviorRelay
import com.vkluchak.doittaskmanager.api.*
import io.reactivex.Completable
import io.reactivex.Single


interface TaskRepository {

    fun getTasksPage(pageNumber: String, sort: String? = ""): Single<TaskPageResponse>

    fun createTask(dueBy: Long, priority: String, title: String): Completable
    fun getTask(taskId: Int): Single<Task>
    fun deleteTask(taskId: Int): Completable

    fun setSortTasksParam(sort: Pair<String, String>)
    fun updateTask(taskId: Int, dueBy: Long, priority: String, taskTitle: String): Completable
}

class TaskRepositoryImpl(private val api: TaskApiService) : TaskRepository {
    override fun updateTask(taskId: Int, dueBy: Long, priority: String, taskTitle: String) =
        api.updateTask(taskId, TaskRequest((dueBy / 1000L), priority, taskTitle)).toCompletable()

    private val sortTasksReplayRelay: BehaviorRelay<String> = BehaviorRelay.create()

    override fun setSortTasksParam(sort: Pair<String, String>) {
        sortTasksReplayRelay.accept("${sort.first} ${sort.second}")
    }

    override fun deleteTask(taskId: Int) =
        api.deleteTask(taskId)

    override fun createTask(dueBy: Long, priority: String, title: String) =
        api.createTask(TaskRequest((dueBy / 1000L), priority, title))
            .toCompletable()

    override fun getTask(taskId: Int) =
        api.getTask(taskId)
            .map { it.task }

    override fun getTasksPage(pageNumber: String, sort: String?) =
        api.getTasksPage(pageNumber, sortTasksReplayRelay.value.orEmpty())


}