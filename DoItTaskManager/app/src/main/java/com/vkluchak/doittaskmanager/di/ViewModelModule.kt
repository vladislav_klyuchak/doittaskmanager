/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vkluchak.doittaskmanager.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vkluchak.doittaskmanager.ui.create.CreateTaskViewModel
import com.vkluchak.doittaskmanager.ui.edit.TaskEditViewModel
import com.vkluchak.doittaskmanager.ui.info.TaskInfoViewModel
import com.vkluchak.doittaskmanager.ui.list.TaskListViewModel
import com.vkluchak.doittaskmanager.ui.login.LoginViewModel
import com.vkluchak.doittaskmanager.viewmodel.TaskViewModelFactory

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(TaskInfoViewModel::class)
    abstract fun bindTaskInfoViewModel(taskInfoViewModel: TaskInfoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TaskEditViewModel::class)
    abstract fun bindTaskEditViewModel(taskEditViewModel: TaskEditViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TaskListViewModel::class)
    abstract fun bindTaskViewModel(taskListViewModel: TaskListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(repoViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateTaskViewModel::class)
    abstract fun bindCreateViewModel(createTaskViewModel: CreateTaskViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: TaskViewModelFactory): ViewModelProvider.Factory
}
