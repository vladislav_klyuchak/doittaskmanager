package com.vkluchak.doittaskmanager.ui.login

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.ReplayRelay
import com.vkluchak.doittaskmanager.repository.SessionRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val sessionRepository: SessionRepository) : ViewModel(){


    val errorReplayRelay: ReplayRelay<String> = ReplayRelay.create()
    val successProfileRelay: PublishRelay<Boolean> = PublishRelay.create()

    fun launchUser(userEmail: String, userPassword: String, isLogin: Boolean) {
        if (isLogin) {
            sessionRepository.login(userEmail, userPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    errorReplayRelay.accept(it.message)
                    successProfileRelay.accept(false)
                }
                .doOnComplete { successProfileRelay.accept(true) }
                .subscribe()
        } else {
            sessionRepository.register(userEmail, userPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    errorReplayRelay.accept(it.message)
                    successProfileRelay.accept(false)
                }
                .doOnComplete { successProfileRelay.accept(true) }
                .subscribe()
        }
    }
}