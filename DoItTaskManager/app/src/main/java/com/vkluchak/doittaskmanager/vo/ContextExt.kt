package com.vkluchak.doittaskmanager.vo

import android.content.Context
import android.widget.Toast


fun Context.showToast(msgId: Int) {
    showToast(this.getString(msgId), Toast.LENGTH_SHORT)
}

fun Context.showToast(msgId: Int, length: Int) {
    showToast(this.getString(msgId), length)
}

fun Context.showToast(msg: String) {
    showToast(msg, Toast.LENGTH_SHORT)
}

fun Context.showToast(msg: String, length: Int) {
    Toast.makeText(this, msg, length).show()
}

