package com.vkluchak.doittaskmanager.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxrelay2.ReplayRelay
import com.vkluchak.doittaskmanager.R
import com.vkluchak.doittaskmanager.api.Task
import com.vkluchak.doittaskmanager.databinding.ItemTaskBinding
import com.vkluchak.doittaskmanager.vo.getDate

class TaskAdapter(private val retryCallback: () -> Unit) : PagedListAdapter<Task, TaskViewHolder>(TaskDiffCallback) {

    val onClickSubject = ReplayRelay.create<Task>()!!

    fun onClickSubjectRelay() = this.onClickSubject

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemTaskBinding =
            DataBindingUtil.inflate(inflater, R.layout.item_task, parent, false)
        return TaskViewHolder(binding)

    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onClickSubject.accept(getItem(position))
        }
        when (getItemViewType(position)) {
            R.layout.item_task -> holder.bindTo(getItem(position)!!)
        }
    }

    override fun getItemViewType(position: Int): Int {
       return R.layout.item_task
    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }

    companion object {
        val TaskDiffCallback = object : DiffUtil.ItemCallback<Task>() {
            override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
                return oldItem == newItem
            }
        }
    }

}

class TaskViewHolder(val binding: ItemTaskBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(task: Task) {
        binding.task = task
        binding.tvTaskTime.text ="Due to ${getDate(task.dueBy)}"
    }
}