package com.vkluchak.doittaskmanager.api.network.interceptor

import com.vkluchak.doittaskmanager.repository.SessionRepositoryImp
import com.vkluchak.doittaskmanager.repository.TOKEN_FIELD_HEADER_NAME
import com.vkluchak.doittaskmanager.repository.auth.ActiveSession
import com.vkluchak.doittaskmanager.repository.auth.Token
import okhttp3.Headers
import okhttp3.Request
import okhttp3.Response

/**
 * This Interceptor designed for management headers:
 * add and update accessToken, add headers for all calls to server
 * */

class SessionInterceptor(private val sessionRepository: SessionRepositoryImp) : RequestChainHandler,
    ResponseChainHandler {

    override fun canHandle(response: Response) = true

    override fun canHandle(request: Request) = true

    override fun chain(builder: Response.Builder): Response.Builder {
        val current = sessionRepository.session.blockingGet() as? ActiveSession

        if (current != null) {
            updateSession(builder.build(), current)
        }

        return builder
    }

    override fun chain(builder: Request.Builder): Request.Builder {
        val session = sessionRepository.session.blockingGet() as? ActiveSession

        if (session != null) {
            // append authorization headers to request
            return builder
                .header(TOKEN_FIELD_HEADER_NAME, session.accessToken.value)

        }

        return builder
    }

    private fun updateSession(response: Response, current: ActiveSession) {

        if (current != null) {
            val headers = response.headers()!!

            if (needToUpdate(headers, current)) {
                sessionRepository.update(current.copy(accessToken = Token(
                    headers[TOKEN_FIELD_HEADER_NAME]!!
                )
                ))
                    .blockingAwait()
            }
        }
    }

    private fun needToUpdate(headers: Headers, session: ActiveSession): Boolean {
        if (!headers.names().contains(TOKEN_FIELD_HEADER_NAME)) {
            return false
        }

        return headers[TOKEN_FIELD_HEADER_NAME] != session?.accessToken?.value
    }

}