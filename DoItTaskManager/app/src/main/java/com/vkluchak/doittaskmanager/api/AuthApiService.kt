package com.vkluchak.doittaskmanager.api

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthApiService {

    @FormUrlEncoded
    @POST("api/users")
    fun users(
        @Field("email") userEmail: String,
        @Field("password") userPassword: String): Single<TokenResponse>

    @FormUrlEncoded
    @POST("api/auth")
    fun auth(
        @Field("email") userEmail: String,
        @Field("password") userPassword: String): Single<TokenResponse>

}


data class SignUpRequestBody(
    @SerializedName("email") val userEmail: String,
    @SerializedName("password") val userPassword: String
)