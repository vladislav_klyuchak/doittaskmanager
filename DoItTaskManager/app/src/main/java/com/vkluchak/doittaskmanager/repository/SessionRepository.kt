package com.vkluchak.doittaskmanager.repository

import android.content.SharedPreferences
import com.vkluchak.doittaskmanager.api.AuthApiService
import com.vkluchak.doittaskmanager.repository.auth.ActiveSession
import com.vkluchak.doittaskmanager.repository.auth.InactiveSession
import com.vkluchak.doittaskmanager.repository.auth.Session
import com.vkluchak.doittaskmanager.repository.auth.Token
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

/**
 * Basic session repository implementation.
 * This implementation appends authorization headers if necessary
 * and allows you to create, destroy and cache session
 */

internal const val TOKEN_FIELD_KEY = "com.vkluchak.taskmanager"
internal const val TOKEN_FIELD_HEADER_NAME = "Authorization"

internal const val ERROR_NOT_REGISTERED = "User not registered"


data class AuthResult(val session: ActiveSession)

interface SessionRepository {

    fun login(userEmail: String, userPassword: String): Completable

    fun register(userEmail: String, userPassword: String): Completable

    val session: Single<out Session>
    fun update(session: Session): Completable
}

class SessionRepositoryImp(
    private val api: AuthApiService,
    private val sharedPreferences: SharedPreferences
) : SessionRepository {

    override fun login(userEmail: String, userPassword: String) =
        api.auth(userEmail, userPassword)
            .map {
                _session = ActiveSession(
                    Token((it.token))
                )
            }.toCompletable()

    override fun register(userEmail: String, userPassword: String) =
        api.users(userEmail, userPassword)
            .map {
                _session = ActiveSession(
                    Token((it.token))
                )
            }.toCompletable()

    private val sessionSubj = PublishSubject.create<Session>()

    private var _session: Session
        set(value) {
            if (value is ActiveSession) {
                //TODO SAVE IN BD, refactor
                sharedPreferences.edit().putString(TOKEN_FIELD_KEY, "Barer ${value.accessToken.value}").apply()
                sessionSubj.onNext(value)
            } else {
                sharedPreferences.edit().remove(TOKEN_FIELD_KEY).apply()
                sessionSubj.onNext(value)
            }
        }
        get() =
            sharedPreferences.getString(TOKEN_FIELD_KEY, null)?.let {
                ActiveSession.newSession(
                    Token(
                        it
                    )
                )
            } ?: InactiveSession.INSTANCE


    override val session: Single<out Session>
        get() = Single.defer { Single.just(_session) }
            .subscribeOn(Schedulers.io())

    override fun update(session: Session): Completable =
        Completable.defer { Completable.fromAction { _session = session } }
            .subscribeOn(Schedulers.io())
}