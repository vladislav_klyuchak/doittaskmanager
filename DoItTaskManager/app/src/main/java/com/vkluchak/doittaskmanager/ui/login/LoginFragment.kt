package com.vkluchak.doittaskmanager.ui.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.vkluchak.doittaskmanager.di.Injectable
import com.vkluchak.doittaskmanager.R
import com.vkluchak.doittaskmanager.databinding.FragmentLoginBinding
import com.vkluchak.doittaskmanager.vo.showToast
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class LoginFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentLoginBinding

    private lateinit var model: LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = Navigation.findNavController(view)

        model = ViewModelProviders.of(this, viewModelFactory)
            .get(LoginViewModel::class.java)

        model.errorReplayRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                context?.showToast("Error \n $it")
            }.subscribe()

        model.successProfileRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                if (it) {
                    navController.navigate(R.id.action_login_screen_to_main)
                }
            }.subscribe()

        setUpViews()
    }

    private fun setUpViews() {
        binding.tieEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                validateEmailView(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.tieEmail.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                validateEmailView((v as EditText).text.toString())
            }
        }

        binding.tiePassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                validatePasswordView(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })


        binding.tiePassword.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                validatePasswordView((v as EditText).text.toString())
            }
        }

        binding.btnLogin.setOnClickListener {
            model.launchUser(
                binding.tieEmail.text.toString(),
                binding.tiePassword.text.toString(),
                !binding.swIsLogin.isChecked
            )
        }
    }

    private fun validatePasswordView(s: String) {
        binding.btnLogin.isEnabled = false
        if (!s.isBlank()) {
            binding.tiePassword.error = null
            binding.btnLogin.isEnabled = true
        } else {
            binding.tiePassword.error = getString(R.string.edit_text_empty_error)
        }
    }

    private fun validateEmailView(s: String) {
        binding.btnLogin.isEnabled = false
        if (!s.isBlank()) {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(s).matches()) {
                binding.tilEmail.error = getString(R.string.edit_text_email_error)
            } else {
                binding.tilEmail.error = null
                binding.btnLogin.isEnabled = true
            }
        } else {
            binding.tilEmail.error = getString(R.string.edit_text_empty_error)
        }
    }
}