package com.vkluchak.doittaskmanager.ui.info

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.vkluchak.doittaskmanager.databinding.FragmentTaskInfoBinding
import com.vkluchak.doittaskmanager.di.Injectable
import com.vkluchak.doittaskmanager.vo.showToast
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject
import android.view.*
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import com.vkluchak.doittaskmanager.R
import com.vkluchak.doittaskmanager.vo.getDate


internal const val ARG_TASK_ID = "arg_task_id"

class TaskInfoFragment : Fragment(), Injectable {

    private lateinit var navController: NavController
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var taskId: Int = 0
    private lateinit var model: TaskInfoViewModel
    private lateinit var binding: FragmentTaskInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        arguments?.let {
            taskId = it.getInt(ARG_TASK_ID)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTaskInfoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_info, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.editTaskAction -> {
                val bundle = bundleOf(ARG_TASK_ID to taskId)
                navController.navigate(R.id.action_info_screen_to_edit, bundle)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        model = ViewModelProviders.of(this, viewModelFactory)
            .get(TaskInfoViewModel::class.java)

        model.errorReplayRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                context?.showToast("Error \n $it")
            }.subscribe()

        model.taskProfileRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                binding.task = it
                binding.tvTaskTime.text ="Due to ${getDate(it.dueBy)}"
            }.subscribe()

        model.deleteStatusRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                navController.popBackStack()
            }.subscribe()
        model.getTask(taskId)

        binding.btnDeleteEvent.setOnClickListener {
            model.deleteTask(taskId)
        }

    }

}