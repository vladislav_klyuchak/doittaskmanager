package com.vkluchak.doittaskmanager

import android.app.Activity
import android.app.Application
import com.facebook.stetho.Stetho
import com.vkluchak.doittaskmanager.di.AppInjector
import com.vkluchak.doittaskmanager.di.RxConfigModule
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        initLogger()
        Stetho.initializeWithDefaults(this)

        AppInjector.init(this)

        RxConfigModule().configure()
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun activityInjector() = dispatchingAndroidInjector
}