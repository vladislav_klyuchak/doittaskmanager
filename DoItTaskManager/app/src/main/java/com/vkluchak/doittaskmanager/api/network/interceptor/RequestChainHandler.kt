package com.vkluchak.doittaskmanager.api.network.interceptor

import okhttp3.Request

interface RequestChainHandler {

    fun canHandle(request: Request): Boolean

    fun chain(builder: Request.Builder) : Request.Builder

}