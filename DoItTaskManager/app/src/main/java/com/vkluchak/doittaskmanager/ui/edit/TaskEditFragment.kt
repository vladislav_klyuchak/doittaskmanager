package com.vkluchak.doittaskmanager.ui.edit

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.vkluchak.doittaskmanager.R
import com.vkluchak.doittaskmanager.databinding.FragmentTaskEditBinding
import com.vkluchak.doittaskmanager.di.Injectable
import com.vkluchak.doittaskmanager.ui.info.ARG_TASK_ID
import com.vkluchak.doittaskmanager.vo.getDueByTimeSeconds
import com.vkluchak.doittaskmanager.vo.initDateTimeViewsValues
import com.vkluchak.doittaskmanager.vo.showToast
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class TaskEditFragment : Fragment(), Injectable {

    private lateinit var navController: NavController
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var taskId: Int = 0
    private lateinit var model: TaskEditViewModel
    private lateinit var binding: FragmentTaskEditBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            taskId = it.getInt(ARG_TASK_ID)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTaskEditBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        model = ViewModelProviders.of(this, viewModelFactory)
            .get(TaskEditViewModel::class.java)

        subscribeToModelData(Navigation.findNavController(view))

        model.getTask(taskId)

        setUpViews()
    }

    private fun setUpViews() {
        binding.dpTaskDate.findViewById<View>(
            Resources.getSystem()
                .getIdentifier("year", "id", "android")
        ).visibility = View.GONE

        binding.tpTaskTime.setIs24HourView(true)

        binding.btnUpdateEvent.setOnClickListener {
            val priority = when (binding.rgTaskPriority.checkedRadioButtonId) {
                R.id.rbHigh -> "High"
                R.id.rbMedium -> "Normal"
                R.id.rbLow -> "Low"
                else -> "Low"
            }

            val pikedTime = getDueByTimeSeconds(binding.dpTaskDate, binding.tpTaskTime)
            if(pikedTime > System.currentTimeMillis())
                model.updateTask(
                    binding.task!!.id,
                    pikedTime,
                    priority,
                    binding.etTaskTitle.text.toString()
                )else {
                context?.showToast("Task time should be greater than the current ")
            }
        }
    }

    private fun subscribeToModelData(navController : NavController) {
        model.errorReplayRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                context?.showToast("Error \n $it")
            }.subscribe()

        model.taskProfileRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                binding.task = it
                when (it.priority) {
                    "High" -> binding.rbHigh.isChecked = true
                    "Normal" -> binding.rbMedium.isChecked = true
                    "Low" -> binding.rbLow.isChecked = true
                    else -> binding.rbLow.isChecked = true
                }

                initDateTimeViewsValues(binding.dpTaskDate, binding.tpTaskTime, it.dueBy)

            }.subscribe()

        model.updatedStatusRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                navController.popBackStack()
            }.subscribe()
    }
}