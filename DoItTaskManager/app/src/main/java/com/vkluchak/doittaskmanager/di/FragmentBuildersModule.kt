/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vkluchak.doittaskmanager.di

import com.vkluchak.doittaskmanager.ui.create.CreateTaskFragment
import com.vkluchak.doittaskmanager.ui.edit.TaskEditFragment
import com.vkluchak.doittaskmanager.ui.info.TaskInfoFragment
import com.vkluchak.doittaskmanager.ui.list.TaskListFragment
import com.vkluchak.doittaskmanager.ui.login.LoginFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeTasksFragment(): TaskListFragment

    @ContributesAndroidInjector
    abstract fun contributeCreateTasksFragment(): CreateTaskFragment

    @ContributesAndroidInjector
    abstract fun contributeTaskInfoFragment(): TaskInfoFragment

    @ContributesAndroidInjector
    abstract fun contributeTaskEditFragment(): TaskEditFragment

}
