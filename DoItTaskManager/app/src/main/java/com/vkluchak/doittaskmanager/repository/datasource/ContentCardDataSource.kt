package com.vkluchak.doittaskmanager.repository.datasource

import androidx.paging.PageKeyedDataSource
import com.vkluchak.doittaskmanager.api.Task
import com.vkluchak.doittaskmanager.repository.TaskRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * A data source that uses the before/after keys returned in page requests.
 * <p>
 * See ItemKeyedSubredditDataSource
 */
class PageKeyedTaskDataSource(
    private val taskRepository: TaskRepository,
    private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Int, Task>() {

    /**
     * Keep Completable reference for the retry event
     */
    private var retryCompletable: Completable? = null

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, Task>
    ) {
        // ignored, since we only ever append to our initial load
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Task>
    ) {
        //get the initial users from the api
        compositeDisposable.add(
            taskRepository.getTasksPage("1")
                .subscribe({ tasks ->
                    // clear retry since last request succeeded
                    setRetry(null)
                    callback.onResult(tasks.tasks, tasks.meta.current, tasks.meta.current + 1)
                }, {
                    // keep a Completable for future retry
                    setRetry(Action { loadInitial(params, callback) })
                })
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Task>) {
        compositeDisposable.add(
            taskRepository.getTasksPage(params.key.toString())
                .subscribe({ it ->
                    // clear retry since last request succeeded
                    setRetry(null)
                    callback.onResult(it.tasks, params.key + 1)
                }, {
                    // keep a Completable for future retry
                    setRetry(Action { loadAfter(params, callback) })
                })
        )
    }

    fun retry() {
        retryCompletable?.let {
            compositeDisposable.add(
                retryCompletable!!
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ }, { throwable ->
                        Timber.e(throwable.message)
                    })
            )
        }
    }


    private fun setRetry(action: Action?) {
        if (action == null) {
            this.retryCompletable = null
        } else {
            this.retryCompletable = Completable.fromAction(action)
        }
    }
}