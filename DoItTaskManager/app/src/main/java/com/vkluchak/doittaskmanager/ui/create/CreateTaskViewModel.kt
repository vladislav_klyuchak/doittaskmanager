package com.vkluchak.doittaskmanager.ui.create

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.ReplayRelay
import com.vkluchak.doittaskmanager.repository.TaskRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CreateTaskViewModel @Inject constructor(private val taskRepository: TaskRepository)  : ViewModel() {


    val errorReplayRelay: ReplayRelay<String> = ReplayRelay.create()
    val successProfileRelay: PublishRelay<Boolean> = PublishRelay.create()

    fun createTask(dueBy: Long, priority: String, title: String) {
        taskRepository.createTask(dueBy, priority, title).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                errorReplayRelay.accept(it.message)
            }
            .doOnComplete { successProfileRelay.accept(true) }
            .subscribe()
    }


}