package com.vkluchak.doittaskmanager.api.network.di

import com.vkluchak.doittaskmanager.api.network.interceptor.RequestChainHandler
import com.vkluchak.doittaskmanager.api.network.interceptor.ResponseChainHandler
import com.vkluchak.doittaskmanager.api.network.interceptor.AppInterceptor
import dagger.Module
import dagger.Provides
import dagger.multibindings.ElementsIntoSet
import javax.inject.Singleton

@Module
class InterceptorModule {

    @Provides
    @ElementsIntoSet
    @Singleton
    internal fun requestHandlersSet(): MutableSet<RequestChainHandler> = mutableSetOf()

    @Provides
    @ElementsIntoSet
    @Singleton
    internal fun responseHandlersSet(): MutableSet<ResponseChainHandler> = mutableSetOf()

    @Provides
    @Singleton
    internal fun provideAppInterceptor(set: MutableSet<RequestChainHandler>): AppInterceptor =
        AppInterceptor(set)

}