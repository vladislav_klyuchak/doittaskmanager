package com.vkluchak.doittaskmanager.repository.di

import android.content.SharedPreferences
import com.vkluchak.doittaskmanager.api.AuthApiService
import com.vkluchak.doittaskmanager.api.TaskApiService
import com.vkluchak.doittaskmanager.api.network.interceptor.AppInterceptor
import com.vkluchak.doittaskmanager.api.network.interceptor.SessionInterceptor
import com.vkluchak.doittaskmanager.repository.SessionRepository
import com.vkluchak.doittaskmanager.repository.SessionRepositoryImp
import com.vkluchak.doittaskmanager.repository.TaskRepository
import com.vkluchak.doittaskmanager.repository.TaskRepositoryImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    internal fun provideSessionRepository(mainInterceptor: AppInterceptor, retrofit: Retrofit, sharedPreferences: SharedPreferences): SessionRepository {

        val sessionRepository = SessionRepositoryImp(retrofit.create(AuthApiService::class.java), sharedPreferences)

        val s = SessionInterceptor(sessionRepository)

        mainInterceptor.addInterceptor(s)

        return sessionRepository
    }

    @Provides
    @Singleton
    internal fun provideTaskRepository(retrofit: Retrofit): TaskRepository {
        val taskRepository = TaskRepositoryImpl(retrofit.create(TaskApiService::class.java))

        return taskRepository

    }
}