package com.vkluchak.doittaskmanager.api

import com.google.gson.annotations.SerializedName
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*


/**
 * REST API access points
 */
interface TaskApiService {

    @GET("api/tasks")
    fun getTasksPage(
        @Query("page") pageNumber: String,
        @Query("sort") sort: String
    ): Single<TaskPageResponse>

    @GET("api/tasks/{task}")
    fun getTask(@Path("task") taskId: Int): Single<TaskResponse>

    @POST("api/tasks")
    fun createTask(@Body taskRequest: TaskRequest): Single<TaskResponse>

    @PUT("api/tasks/{task}")
    fun updateTask(@Path("task") taskId: Int, @Body taskUpdateRequest: TaskRequest): Single<ArrayList<TaskResponse>>


    @DELETE("api/tasks/{task}")
    fun deleteTask(@Path("task") taskId: Int): Completable//TODO add params

}

data class TokenResponse(
    @SerializedName("token") val token: String
)

data class TaskRequest(
    @SerializedName("dueBy")  val dueBy: Long,
    @SerializedName("priority")   val priority: String,
    @SerializedName("title")  val title: String
)

data class TaskResponse(
    @SerializedName("task") val task: Task
)

data class Task(
    @SerializedName("dueBy") val dueBy: Long,
    @SerializedName("id") val id: Int,
    @SerializedName("priority") val priority: String,
    @SerializedName("title") val title: String
)

data class TaskPageResponse(
    val meta: Meta,
    val tasks: List<Task>
)

data class Meta(
    val count: Int,
    val current: Int,
    val limit: Int
)